package utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CSVWriter {
    private File file;
    private String separator = ";";

    public CSVWriter(File file) {
        this.file = file;
    }

    public CSVWriter(File file, String separator) {
        this(file);
        this.separator = separator;
    }

    public void write(String content) {
        if (!file.exists()) {
            try {
                if (!file.createNewFile()) return;
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
        try {
            FileWriter fileWriter = new FileWriter(this.file, false);
            fileWriter.append(content).flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(String[] columnsName, Object[][] data) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.join(this.separator, columnsName) + "\n");
        for (Object[] d : data) {
            String[] stringData = new String[d.length];
            for (int i = 0; i < d.length; i++) stringData[i] = d[i].toString();
            stringBuilder.append(String.join(this.separator, stringData) + "\n");
        }
        this.write(stringBuilder.toString());
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
