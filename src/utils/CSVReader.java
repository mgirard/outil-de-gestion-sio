package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

public class CSVReader {
    private File file;
    private List<String> lines;
    private String separator = ";";

    public CSVReader(File file) throws IOException, FileNotFoundException {
        if (!file.exists()) throw new FileNotFoundException();
        this.file = file;
        this.lines = Files.readAllLines(file.toPath());
    }

    public CSVReader(File file, String separator) throws IOException, FileNotFoundException {
        if (!file.exists()) throw new FileNotFoundException();
        this.file = file;
        this.separator = separator;
        this.lines = Files.readAllLines(file.toPath());
    }

    public CSVReader(String content) {
        this.lines = Arrays.asList(content.split("\n"));
    }

    public CSVReader(String content, String separator) {
        this.lines = Arrays.asList(content.split("\n"));
        this.separator = separator;
    }

    /**
     * Return first line of content
     *
     * @return
     */
    public String getFirstLine() {
        if (this.lines.size() > 0) return this.lines.get(0);
        return null;
    }

    /**
     * Return fields of content
     *
     * @return
     */
    public ArrayList<String> getFields() {
        ArrayList<String> fields = null;
        if (this.getFirstLine() != null)
            fields = new ArrayList<>(Arrays.asList(this.getFirstLine().split(this.separator)));
        return fields;
    }

    /**
     * Return content mapped by fields
     */
    public Map<String, ArrayList<String>> getMappedByFields() {
        Map<String, ArrayList<String>> map = null;
        if (this.getFirstLine() != null) {
            map = new HashMap<>();
            ArrayList<String> fields = this.getFields();
            for (String field : fields) map.put(field, new ArrayList<>());
            List<String> lines = this.getLines();
            lines.remove(0);
            for (String line : lines) {
                String[] splitLine = line.split(this.separator);
                for (int i = 0; i < splitLine.length; i++) map.get(fields.get(i)).add(splitLine[i]);
            }
        }
        return map;
    }

    public ArrayList<Map<String, String>> getValues() {
        ArrayList<Map<String, String>> values = new ArrayList<>();
        List<String> fields = this.getFields();
        List<String> lines = this.getLines();
        lines.remove(0);
        for (String line : lines) {
            HashMap<String, String> map = new HashMap<>();
            String[] split = line.split(this.getSeparator());
            for (int i = 0; i < fields.size(); i++) {
                String value = null;
                try {
                    value = split[i];
                } catch (Exception e) {
                }
                map.put(Utils.normalize(fields.get(i)).toLowerCase(), value);
            }
            values.add(map);
        }
        return values;
    }

    /**
     * Get file
     *
     * @return
     */
    public File getFile() {
        return file;
    }

    /**
     * Get content of file
     *
     * @return
     */
    public String getContent() {
        return String.join("\n", this.lines);
    }

    public List<String> getLines() {
        return lines;
    }

    /**
     * Get reader separator
     *
     * @return
     */
    public String getSeparator() {
        return separator;
    }

    /**
     * Set reader separator
     *
     * @param separator
     */
    public void setSeparator(String separator) {
        this.separator = separator;
    }
}
