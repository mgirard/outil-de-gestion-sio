package data;

import utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DataChecker {
    public static boolean checkData(ArrayList<String> fields, DataType type) {
        HashMap<String, Boolean> required = DataChecker.getRequireFieldByType(type);
        for (String field : fields) {
            String normalized = Utils.normalize(field).toLowerCase();
            if (required.containsKey(normalized)) required.replace(normalized, true);
        }
        for (Map.Entry<String, Boolean> entry : required.entrySet()) if (!entry.getValue()) return false;
        return true;
    }

    private static HashMap<String, Boolean> getRequireFieldByType(DataType type) {
        HashMap<String, Boolean> required = new HashMap<>();
        switch (type) {
            case PERSONNE:
                required.put("nom", false);
                required.put("prenom", false);
                break;
            case ETUDIANT:
            case ENSEIGNANT:
            case PROFESSIONNEL:
                required = DataChecker.getRequireFieldByType(DataType.PERSONNE);
                break;
            default:
                return null;
        }
        return required;
    }
}
