package data;

public enum DataType {
    PERSONNE("personne", "Personnes"),
    ETUDIANT("etudiant", "Etudiants"),
    ETUDIANT_CONTACT("etudiant_contact", "Stagiaire encadrées par un contact"),
    ETUDIANT_ORGANISATION("etudiant_organisation", "Stagiaire encadrées par une organisation"),
    ENSEIGNANT("enseignant", "Enseignants"),
    PROFESSIONNEL("professionnel", "Professionnels"),
    PROFESSIONNEL_JURY_SISR("professionnel_jury_sisr", "Contacts ayant donnés leur accord pour participer au jury pour la spécialité SISR"),
    PROFESSIONNEL_STAGIAIRE_SISR_1er("professionnel_stagiaire_sisr_1er", "Contacts ayant donnés leur accord de principe pour accueillir un stagiaire pour la spécialité SISR pour les 1ère années"),
    PROFESSIONNEL_STAGIAIRE_SISR_2eme("professionnel_stagiaire_sisr_2eme", "Contacts ayant donnés leur accord de principe pour accueillir un stagiaire pour la spécialité SISR pour les 2ème années"),
    PROFESSIONNEL_JURY_SLAM("professionnel_jury_slam", "Contacts ayant donnés leur accord pour participer au jury pour la spécialité SLAM"),
    PROFESSIONNEL_STAGIAIRE_SLAM_1er("professionnel_stagiaire_slam_1er", "Contacts ayant donnés leur accord de principe pour accueillir un stagiaire pour la spécialité SLAM pour les 1ère année"),
    PROFESSIONNEL_STAGIAIRE_SLAM_2eme("professionnel_stagiaire_slam_2eme", "Contacts ayant donnés leur accord de principe pour accueillir un stagiaire pour la spécialité SLAM pour les 2ème année"),
    STAGE("stage", "Stages"),
    ENTREPRISE("entreprise", "Entreprises"),
    VISITE("visite", "Visites");

    private String key;
    private String name;

    DataType(String key, String name) {
        this.key = key;
        this.name = name;
    }

    public String toString() {
        return this.name;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }
}
