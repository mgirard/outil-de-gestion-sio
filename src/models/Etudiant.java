package models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Etudiant implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private Personne personne;
    @ManyToOne
    private Classe classe;
    @ManyToOne
    private Option option;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    public Classe getClasse() {
        return classe;
    }

    public void setClasse(Classe classe) {
        this.classe = classe;
    }

    public Option getOption() {
        return option;
    }

    public void setOption(Option option) {
        this.option = option;
    }
}
