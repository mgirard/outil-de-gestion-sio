package models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = EtudiantClasse.TABLE_NAME)
public class EtudiantClasse implements Serializable {
    public static final String TABLE_NAME = "etudiant_classe";

    @Id
    @ManyToOne
    private Etudiant etudiant;
    @Id
    @ManyToOne
    private Classe classe;
    @Temporal(TemporalType.DATE)
    private Date annee;

    public Etudiant getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }

    public Classe getClasse() {
        return classe;
    }

    public void setClasse(Classe classe) {
        this.classe = classe;
    }

    public Date getAnnee() {
        return annee;
    }

    public void setAnnee(Date annee) {
        this.annee = annee;
    }
}