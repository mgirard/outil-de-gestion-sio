package models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = EmploiMaitreStage.TABLE_NAME)
public class EmploiMaitreStage implements Serializable {
    public static final String TABLE_NAME = "emploi_maitre_stage";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private Professionnel professionnel;
    @OneToOne
    private Entreprise entreprise;
    @Column(length = 255)
    private String specialite;
    @Column(length = 255)
    private String metier;
    @Temporal(TemporalType.DATE)
    private Date annee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Professionnel getPersonne() {
        return professionnel;
    }

    public void setPersonne(Professionnel professionnel) {
        this.professionnel = professionnel;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }

    public String getSpecialite() {
        return specialite;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    public String getMetier() {
        return metier;
    }

    public void setMetier(String metier) {
        this.metier = metier;
    }

    public Date getDate() {
        return annee;
    }

    public void setDate(Date date) {
        this.annee = date;
    }
}