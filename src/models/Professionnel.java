package models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Professionnel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private Personne personne;
    @Column(length = 1)
    private int ancienEleve;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    public int getAncienEleve() {
        return ancienEleve;
    }

    public void setAncienEleve(int ancienEleve) {
        this.ancienEleve = ancienEleve;
    }
}
