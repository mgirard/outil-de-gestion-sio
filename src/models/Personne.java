package models;

import main.GestionSIO;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Personne implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 255)
    private String nom;
    @Column(length = 255)
    private String prenom;
    @Column(length = 255)
    private String email;
    @Column(length = 255)
    private String telephone;
    @Column(length = 255)
    private String civilite;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    public static Personne findOneById(int id) {
        EntityManager entityManager = GestionSIO.getEntityManager();
        Query query = entityManager.createQuery("SELECT p FROM Personne p WHERE p.id = :id")
                .setParameter("id", id);
        if (query.getResultList().size() > 0 && query.getSingleResult() != null)
            return (Personne) query.getSingleResult();
        return null;
    }
}
