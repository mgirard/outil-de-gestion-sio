package models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Visite implements Serializable{

    @Id
    @GeneratedValue
    private int id;
    @OneToOne
    private Stage stage;
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(length = 255, name = "compte_rendu")
    private String compteRendu;
    @Column(length = 1)
    private int oral;
    @Column(length = 1, name = "dispo_stage_premiere_annee")
    private int dispoStagePremiereAnnee;
    @Column(length = 1, name = "dispo_stage_deuxieme_annee")
    private int dispoStageDeuxiemeAnnee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCompteRendu() {
        return compteRendu;
    }

    public void setCompteRendu(String compteRendu) {
        this.compteRendu = compteRendu;
    }

    public int getOral() {
        return oral;
    }

    public void setOral(int oral) {
        this.oral = oral;
    }

    public int getDispoStagePremiereAnnee() {
        return dispoStagePremiereAnnee;
    }

    public void setDispoStagePremiereAnnee(int dispoStagePremiereAnnee) {
        this.dispoStagePremiereAnnee = dispoStagePremiereAnnee;
    }

    public int getDispoStageDeuxiemeAnnee() {
        return dispoStageDeuxiemeAnnee;
    }

    public void setDispoStageDeuxiemeAnnee(int dispoStageDeuxiemeAnnee) {
        this.dispoStageDeuxiemeAnnee = dispoStageDeuxiemeAnnee;
    }
}
