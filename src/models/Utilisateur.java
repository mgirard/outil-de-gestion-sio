package models;

import main.GestionSIO;
import utils.BCrypt;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Utilisateur implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 255)
    private String pseudo;
    @Column(name = "mot_de_passe")
    private String motDePasse;
    @Column(length = 1)
    private int role;
    @OneToOne
    private Personne personne;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    public static Utilisateur findOneById(int id) {
        EntityManager entityManager = GestionSIO.getEntityManager();
        Query query = entityManager.createQuery("SELECT u FROM Utilisateur u WHERE u.id = :id")
                .setParameter("id", id);
        if (query.getResultList().size() > 0 && query.getSingleResult() != null)
            return (Utilisateur) query.getSingleResult();
        return null;
    }

    public static Utilisateur findOneByPseudo(String pseudo) {
        EntityManager entityManager = GestionSIO.getEntityManager();
        Query query = entityManager.createQuery("SELECT u FROM Utilisateur u WHERE u.pseudo = :pseudo")
                .setParameter("pseudo", pseudo);
        if (query.getResultList().size() > 0 && query.getSingleResult() != null)
            return (Utilisateur) query.getSingleResult();
        return null;
    }

    public static void createUserIfNotExist() {
        EntityManager entityManager = GestionSIO.getEntityManager();
        EntityTransaction entityTransaction = GestionSIO.getEntityTransaction();
        Query query = entityManager.createQuery("SELECT u FROM Utilisateur u WHERE u.role = 2");
        if (query.getResultList().size() <= 0) {
            entityTransaction.begin();
            Utilisateur utilisateur = new Utilisateur();
            utilisateur.setPseudo("admin");
            utilisateur.setMotDePasse(Utilisateur.generatePassword("Nimd@"));
            utilisateur.setRole(2);
            entityManager.persist(utilisateur);
            entityManager.flush();
            entityTransaction.commit();
        }
    }

    public static String generatePassword(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }
}