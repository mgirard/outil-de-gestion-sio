package views;

import javax.swing.*;

public class Load extends AbstractView {
    private JPanel panel;
    private JProgressBar loadingBar;
    private JLabel text;

    public Load() {
        super("Loading");
        this.loadingBar.setIndeterminate(true);
    }

    @Override
    public JPanel getContent() {
        return panel;
    }

    public JProgressBar getLoadingBar() {
        return loadingBar;
    }

    public JLabel getText() {
        return text;
    }
}
