package views;

import javax.swing.*;

public class Register extends AbstractView {
    private JPanel registerView;
    private JButton retourButton;
    private JTextField pseudoField;
    private JPasswordField passwordField;
    private JButton connexionButton;

    public Register() {
        super("S'identifier");
    }

    public JPanel getRegisterView() {
        return registerView;
    }

    public JButton getRetourButton() {
        return retourButton;
    }

    public JTextField getPseudoField() {
        return pseudoField;
    }

    public JPasswordField getPasswordField() {
        return passwordField;
    }

    public JButton getConnexionButton() {
        return connexionButton;
    }

    @Override
    public JPanel getContent() {
        return this.registerView;
    }
}
