package views;

import controller.CsvDataController;
import data.DataType;
import main.GestionSIO;

import javax.swing.*;
import java.awt.*;
import java.io.File;

public class InsertData extends JFrame {
    private JButton chooseButton;
    private JPanel content;
    private JButton insertButton;
    private JComboBox dataType;
    private JButton exportButton;

    private String buttonText = "Choisir le fichier";

    private JFileChooser jFileChooser;

    private File chooseFile = null;

    public InsertData() throws HeadlessException {
        super("Données CSV - " + GestionSIO.APP_NAME);
        this.setContentPane(content);
        this.jFileChooser = new JFileChooser();
        this.insertButton.setEnabled(false);
        this.setPreferredSize(new Dimension(450, 300));
        this.initializeComboBox();
        this.initializeEvents();
        this.pack();
    }

    private void initializeComboBox() {
        this.dataType.addItem(DataType.ETUDIANT);
        this.dataType.addItem(DataType.ENSEIGNANT);
        this.dataType.addItem(DataType.PROFESSIONNEL);
    }

    private void initializeEvents() {
        this.chooseButton.addActionListener(e -> {
            this.jFileChooser.showDialog(this, "Choisir le fichier");
            this.chooseFile = this.jFileChooser.getSelectedFile();
            if (this.chooseFile != null) {
                this.insertButton.setEnabled(true);
                this.chooseButton.setText(this.jFileChooser.getSelectedFile().getName());
            } else {
                this.insertButton.setEnabled(false);
                this.chooseButton.setText(this.buttonText);
            }
        });
        this.insertButton.addActionListener(e -> {
            Boolean result = CsvDataController.insertData(this.chooseFile, (DataType) this.dataType.getSelectedItem());
            if (result)
                JOptionPane.showMessageDialog(this, "Insertion terminée !", "Succès", JOptionPane.INFORMATION_MESSAGE);
            else if (!result)
                JOptionPane.showMessageDialog(this, "Des données sont manquantes dans votre fichier !", "Error", JOptionPane.ERROR_MESSAGE);
            else JOptionPane.showMessageDialog(this, "Une erreur est survenue.", "Error", JOptionPane.ERROR_MESSAGE);
        });
        this.exportButton.addActionListener(e -> {
            this.jFileChooser.setSelectedFile(new File(((DataType) this.dataType.getSelectedItem()).getKey() + ".csv"));
            int choose = this.jFileChooser.showDialog(this, "Choisir le fichier de destination");
            if (choose == JFileChooser.APPROVE_OPTION) {
                CsvDataController.exportData(this.jFileChooser.getSelectedFile(), (DataType) this.dataType.getSelectedItem());
            }
        });
    }
}
