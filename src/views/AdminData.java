package views;

import javax.swing.*;

public class AdminData extends AbstractView {
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JTextField textField5;
    private JComboBox comboBox1;
    private JComboBox comboBox2;
    private JComboBox comboBox3;
    private JComboBox comboBox4;
    private JButton cancelButton;
    private JButton validButton;
    private JButton returnButton;
    private JPanel contentPanel;
    private JButton insertCsvData;

    public AdminData() {
        super("Administration");
    }

    @Override
    public JPanel getContent() {
        return this.contentPanel;
    }

    public JTextField getTextField1() {
        return textField1;
    }

    public JTextField getTextField2() {
        return textField2;
    }

    public JTextField getTextField3() {
        return textField3;
    }

    public JTextField getTextField4() {
        return textField4;
    }

    public JTextField getTextField5() {
        return textField5;
    }

    public JComboBox getComboBox1() {
        return comboBox1;
    }

    public JComboBox getComboBox2() {
        return comboBox2;
    }

    public JComboBox getComboBox3() {
        return comboBox3;
    }

    public JComboBox getComboBox4() {
        return comboBox4;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    public JButton getValidButton() {
        return validButton;
    }

    public JButton getReturnButton() {
        return returnButton;
    }

    public JPanel getContentPanel() {
        return contentPanel;
    }

    public JButton getInsertCsvData() {
        return insertCsvData;
    }
}
