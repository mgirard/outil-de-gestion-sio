package views;

import javax.swing.*;

abstract public class AbstractView {
    private String title;

    public AbstractView(String title) {
        this.title = title;
    }

    public void beforeDisplay() {
    }

    public String getTitle() {
        return title;
    }

    /*
     * Get JPanel view container
     */
    abstract public JPanel getContent();
}
