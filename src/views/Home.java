package views;

import data.DataType;
import main.GestionSIO;
import utils.CSVWriter;

import javax.persistence.EntityManager;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.io.File;
import java.util.List;

import static data.DataType.STAGE;

public class Home extends AbstractView {

    private JPanel homeView;

    private JButton loginButton;
    private JButton searchButton;
    private JTable dataTable;
    private JComboBox categorieComboBox;
    private JComboBox comboBox2;
    private JTextField textFieldSearch;
    private JLabel JLabel1;
    private JScrollPane dataScrollPane;
    private JButton exportationDeLaTableButton;
    private JButton adminButton;

    String loginText = "S'identifier";
    String logoutText = "Déconnexion";

    public Home() {
        super("Home");
    }

    @Override
    public void beforeDisplay() {
        this.updateLoginButton();
        this.updateAdminButton();
        this.setCategorie();
        this.setTable();
    }

    public void setCategorie() {
        this.getCategorieComboBox().removeAllItems();
        this.getCategorieComboBox().addItem(STAGE);
        if (GestionSIO.getUser().getRole() == 1 || GestionSIO.getUser().getRole() == 2) {
            this.getCategorieComboBox().addItem(DataType.ETUDIANT_ORGANISATION);
            this.getCategorieComboBox().addItem(DataType.ETUDIANT_CONTACT);
            this.getCategorieComboBox().addItem(DataType.PROFESSIONNEL_JURY_SISR);
            this.getCategorieComboBox().addItem(DataType.PROFESSIONNEL_JURY_SLAM);
            this.getCategorieComboBox().addItem(DataType.PROFESSIONNEL_STAGIAIRE_SISR_1er);
            this.getCategorieComboBox().addItem(DataType.PROFESSIONNEL_STAGIAIRE_SISR_2eme);
            this.getCategorieComboBox().addItem(DataType.PROFESSIONNEL_STAGIAIRE_SLAM_1er);
            this.getCategorieComboBox().addItem(DataType.PROFESSIONNEL_STAGIAIRE_SLAM_2eme);
        }
        this.getCategorieComboBox().addActionListener(e -> {
            if (this.getCategorieComboBox().getSelectedIndex() != -1) {
                this.setTable();
            }
        });
    }

    public void setTable() {
        if (this.getCategorieComboBox().getSelectedItem() != null) {
            EntityManager entityManager = GestionSIO.getEntityManager();
            String[] columns = null;
            String research = getTextFieldSearch().getText();
            Object[][] data = null;
            switch ((DataType) this.getCategorieComboBox().getSelectedItem()) {
                case ETUDIANT_CONTACT:
                    List<Object[]> etudiants_contact =
                            (List<Object[]>) entityManager.createQuery(
                                    "SELECT pro.personne.nom, pro.personne.prenom, pro.personne.email, pro.personne.telephone ,e.personne.nom, e.personne.prenom, e.personne.telephone, " +
                                            "e.personne.email, ems.specialite, FUNC('YEAR', ems.annee), s.debut, s.fin, s.sujet, " +
                                            "s.techno FROM Stage s " +
                                            "JOIN Etudiant e ON e.id = s.etudiant.id " +
                                            "JOIN Professionnel pro ON pro.id = s.professionnel.id " +
                                            "JOIN Personne pers ON pers.id = pro.personne.id " +
                                            "JOIN EmploiMaitreStage ems ON ems.professionnel.id = pro.id " +
                                            "WHERE pro.personne.nom like :word OR pro.personne.prenom like :word").setParameter("word", "%" + research + "%").getResultList();
                    columns = new String[]{"Nom du Contact", "Prénom du Cnntact", "Email du Contact", "Téléphone du Contact", "Nom de l'étudiant", "Prénom de l'étudiant", "Téléphone de l'étudiant", "Email de l'étudiant", "Spécialité", "Année", "Début", "Fin", "Sujet", "Techno"};
                    data = new Object[etudiants_contact.size()][];
                    for (int i = 0; i < etudiants_contact.size(); i++) data[i] = etudiants_contact.get(i);
                    break;
                case ETUDIANT_ORGANISATION:
                    List<Object[]> etudiants_organisation =
                            (List<Object[]>) entityManager.createQuery(
                                    "SELECT en.nom,pro.personne.nom, pro.personne.prenom, pro.personne.email, pro.personne.telephone ,e.personne.nom, e.personne.prenom, e.personne.telephone, " +
                                            "e.personne.email, ems.specialite, FUNC('YEAR', ems.annee), s.debut, s.fin, s.sujet, " +
                                            "s.techno FROM Stage s " +
                                            "JOIN Etudiant e ON e.id = s.etudiant.id " +
                                            "JOIN Professionnel pro ON pro.id = s.professionnel.id " +
                                            "JOIN Personne pers ON pers.id = pro.personne.id " +
                                            "JOIN EmploiMaitreStage ems ON ems.professionnel.id = pro.id " +
                                            "JOIN Entreprise en ON en.id = s.entreprise.id " +
                                            "WHERE en.nom like :word").setParameter("word", "%" + research + "%").getResultList();
                    columns = new String[]{"Nom de l'entreprise", "Nom du Contact", "Prénom du Cnntact", "Email du Contact", "Téléphone du Contact", "Nom de l'étudiant", "Prénom de l'étudiant", "Téléphone de l'étudiant", "Email de l'étudiant", "Spécialité", "Année", "Début", "Fin", "Sujet", "Techno"};
                    data = new Object[etudiants_organisation.size()][];
                    for (int i = 0; i < etudiants_organisation.size(); i++) data[i] = etudiants_organisation.get(i);
                    break;
                case PROFESSIONNEL_JURY_SISR:
                    List<Object[]> professionnels_jury_sisr = (List<Object[]>) entityManager.createQuery(
                            "SELECT p.nom, p.prenom, p.email, p.telephone, p.civilite, e.nom, e.adresse FROM Visite v " +
                                    "JOIN Stage s ON s.id = v.stage.id " +
                                    "JOIN Professionnel pro ON pro.id = s.professionnel.id " +
                                    "JOIN EmploiMaitreStage ems ON ems.professionnel.id = pro.id " +
                                    "JOIN Personne p ON p.id = pro.personne.id " +
                                    "JOIN Entreprise e ON e.id = s.entreprise.id " +
                                    "WHERE ems.specialite = 'SISR' AND FUNC('YEAR',v.date) = 2020 AND v.oral = 1").getResultList();
                    columns = new String[]{"Nom", "Prénom", "Email", "Téléphone", "Civilité", "Nom de l'entreprise", "Adresse de l'entreprise"};
                    data = new Object[professionnels_jury_sisr.size()][];
                    for (int i = 0; i < professionnels_jury_sisr.size(); i++) data[i] = professionnels_jury_sisr.get(i);
                    break;
                case PROFESSIONNEL_JURY_SLAM:
                    List<Object[]> professionnels_jury_slam = (List<Object[]>) entityManager.createQuery(
                            "SELECT p.nom, p.prenom, p.email, p.telephone, p.civilite, e.nom, e.adresse FROM Visite v " +
                                    "JOIN Stage s ON s.id = v.stage.id " +
                                    "JOIN Professionnel pro ON pro.id = s.professionnel.id " +
                                    "JOIN EmploiMaitreStage ems ON ems.professionnel.id = pro.id " +
                                    "JOIN Personne p ON p.id = pro.personne.id " +
                                    "JOIN Entreprise e ON e.id = s.entreprise.id " +
                                    "WHERE ems.specialite = 'SLAM' AND FUNC('YEAR',v.date) = 2020 AND v.oral = 1").getResultList();
                    columns = new String[]{"Nom", "Prénom", "Email", "Téléphone", "Civilité", "Nom de l'entreprise", "Adresse de l'entreprise"};
                    data = new Object[professionnels_jury_slam.size()][];
                    for (int i = 0; i < professionnels_jury_slam.size(); i++) data[i] = professionnels_jury_slam.get(i);
                    break;
                case PROFESSIONNEL_STAGIAIRE_SISR_1er:
                    List<Object[]> professionnels_stage_sisr_1er = (List<Object[]>) entityManager.createQuery(
                            "SELECT p.nom, p.prenom, p.email, p.telephone, p.civilite, e.nom, e.adresse FROM Visite v " +
                                    "JOIN Stage s ON s.id = v.stage.id " +
                                    "JOIN Professionnel pro ON pro.id = s.professionnel.id " +
                                    "JOIN EmploiMaitreStage ems ON ems.professionnel.id = pro.id " +
                                    "JOIN Personne p ON p.id = pro.personne.id " +
                                    "JOIN Entreprise e ON e.id = s.entreprise.id " +
                                    "WHERE ems.specialite = 'SISR' AND FUNC('YEAR',v.date) = 2020 AND v.dispoStagePremiereAnnee = 1").getResultList();
                    columns = new String[]{"Nom", "Prénom", "Email", "Téléphone", "Civilité", "Nom de l'entreprise", "Adresse de l'entreprise"};
                    data = new Object[professionnels_stage_sisr_1er.size()][];
                    for (int i = 0; i < professionnels_stage_sisr_1er.size(); i++)
                        data[i] = professionnels_stage_sisr_1er.get(i);
                    break;
                case PROFESSIONNEL_STAGIAIRE_SISR_2eme:
                    List<Object[]> professionnels_stage_sisr_2eme = (List<Object[]>) entityManager.createQuery(
                            "SELECT p.nom, p.prenom, p.email, p.telephone, p.civilite, e.nom, e.adresse FROM Visite v " +
                                    "JOIN Stage s ON s.id = v.stage.id " +
                                    "JOIN Professionnel pro ON pro.id = s.professionnel.id " +
                                    "JOIN EmploiMaitreStage ems ON ems.professionnel.id = pro.id " +
                                    "JOIN Personne p ON p.id = pro.personne.id " +
                                    "JOIN Entreprise e ON e.id = s.entreprise.id " +
                                    "WHERE ems.specialite = 'SISR' AND FUNC('YEAR',v.date) = 2020 AND v.dispoStageDeuxiemeAnnee = 1").getResultList();
                    columns = new String[]{"Nom", "Prénom", "Email", "Téléphone", "Civilité", "Nom de l'entreprise", "Adresse de l'entreprise"};
                    data = new Object[professionnels_stage_sisr_2eme.size()][];
                    for (int i = 0; i < professionnels_stage_sisr_2eme.size(); i++)
                        data[i] = professionnels_stage_sisr_2eme.get(i);
                    break;
                case PROFESSIONNEL_STAGIAIRE_SLAM_1er:
                    List<Object[]> professionnels_stage_slam_1er = (List<Object[]>) entityManager.createQuery(
                            "SELECT p.nom, p.prenom, p.email, p.telephone, p.civilite, e.nom, e.adresse FROM Visite v " +
                                    "JOIN Stage s ON s.id = v.stage.id " +
                                    "JOIN Professionnel pro ON pro.id = s.professionnel.id " +
                                    "JOIN EmploiMaitreStage ems ON ems.professionnel.id = pro.id " +
                                    "JOIN Personne p ON p.id = pro.personne.id " +
                                    "JOIN Entreprise e ON e.id = s.entreprise.id " +
                                    "WHERE ems.specialite = 'SLAM' AND FUNC('YEAR',v.date) = 2020 AND v.dispoStagePremiereAnnee = 1").getResultList();
                    columns = new String[]{"Nom", "Prénom", "Email", "Téléphone", "Civilité", "Nom de l'entreprise", "Adresse de l'entreprise"};
                    data = new Object[professionnels_stage_slam_1er.size()][];
                    for (int i = 0; i < professionnels_stage_slam_1er.size(); i++)
                        data[i] = professionnels_stage_slam_1er.get(i);
                    break;
                case PROFESSIONNEL_STAGIAIRE_SLAM_2eme:
                    List<Object[]> professionnels_stage_slam_2eme = (List<Object[]>) entityManager.createQuery(
                            "SELECT p.nom, p.prenom, p.email, p.telephone, p.civilite, e.nom, e.adresse FROM Visite v " +
                                    "JOIN Stage s ON s.id = v.stage.id " +
                                    "JOIN Professionnel pro ON pro.id = s.professionnel.id " +
                                    "JOIN EmploiMaitreStage ems ON ems.professionnel.id = pro.id " +
                                    "JOIN Personne p ON p.id = pro.personne.id " +
                                    "JOIN Entreprise e ON e.id = s.entreprise.id " +
                                    "WHERE ems.specialite = 'SLAM' AND FUNC('YEAR',v.date) = 2020 AND v.dispoStageDeuxiemeAnnee = 1").getResultList();
                    columns = new String[]{"Nom", "Prénom", "Email", "Téléphone", "Civilité", "Nom de l'entreprise", "Adresse de l'entreprise"};
                    data = new Object[professionnels_stage_slam_2eme.size()][];
                    for (int i = 0; i < professionnels_stage_slam_2eme.size(); i++)
                        data[i] = professionnels_stage_slam_2eme.get(i);
                    data = new Object[professionnels_stage_slam_2eme.size()][];
                    for (int i = 0; i < professionnels_stage_slam_2eme.size(); i++)
                        data[i] = professionnels_stage_slam_2eme.get(i);
                    break;
                case STAGE:
                    List<Object[]> stages = (List<Object[]>) entityManager.createQuery("SELECT e.personne.nom, e.personne.prenom,s.debut, s.fin, s.techno, s.sujet FROM Stage s " +
                            "JOIN Etudiant e ON e.id = s.etudiant.id ").getResultList();
                    columns = new String[]{"Nom", "Prénom", "Début", "Fin", "Techno", "Sujet"};
                    data = new Object[stages.size()][];
                    for (int i = 0; i < stages.size(); i++) data[i] = stages.get(i);
                    break;
            }
            if (columns != null && data != null) this.getDataTable().setModel(new DefaultTableModel(data, columns));
        }
    }

    public void exportTable(File file) {
        CSVWriter csvWriter = new CSVWriter(file);
        int cc = this.getDataTable().getColumnCount();
        int rw = this.getDataTable().getRowCount();
        String[] columnsName = new String[cc];
        for (int i = 0; i < cc; i++) columnsName[i] = this.getDataTable().getColumnName(i);
        Object[][] data = new Object[rw][];
        for (int i = 0; i < rw; i++) {
            Object[] rowData = new Object[cc];
            for (int j = 0; j < cc; j++) rowData[j] = this.getDataTable().getValueAt(i, j);
            data[i] = rowData;
        }
        csvWriter.write(columnsName, data);
    }

    public void updateLoginButton() {
        this.loginButton.setText(GestionSIO.getUser().getId() == 0 ? loginText : logoutText);
    }

    public void updateAdminButton() {
        this.adminButton.setVisible(GestionSIO.getUser().getRole() == 2);
    }

    public JTextField getTextFieldSearch() {
        return textFieldSearch;
    }

    public JButton getExportationDeLaTableButton() {
        return exportationDeLaTableButton;
    }

    public JPanel getHomeView() {
        return homeView;
    }

    public void setHomeView(JPanel homeView) {
        this.homeView = homeView;
    }

    public JButton getLoginButton() {
        return loginButton;
    }

    public void setLoginButton(JButton loginButton) {
        this.loginButton = loginButton;
    }

    public JButton getSearchButton() {
        return searchButton;
    }

    public void setSearchButton(JButton searchButton) {
        this.searchButton = searchButton;
    }

    public JTable getDataTable() {
        return dataTable;
    }

    public void setDataTable(JTable dataTable) {
        this.dataTable = dataTable;
    }

    public JComboBox getCategorieComboBox() {
        return categorieComboBox;
    }

    public void setCategorieComboBox(JComboBox categorieComboBox) {
        this.categorieComboBox = categorieComboBox;
    }

    public JComboBox getComboBox2() {
        return comboBox2;
    }

    public void setComboBox2(JComboBox comboBox2) {
        this.comboBox2 = comboBox2;
    }

    @Override
    public JPanel getContent() {
        return this.homeView;
    }

    public JLabel getJLabel1() {
        return JLabel1;
    }

    public void setJLabel1(JLabel JLabel1) {
        this.JLabel1 = JLabel1;
    }

    public JButton getAdminButton() {
        return adminButton;
    }
}
