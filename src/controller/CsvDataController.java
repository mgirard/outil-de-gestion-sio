package controller;

import data.DataChecker;
import data.DataType;
import main.GestionSIO;
import models.Enseignant;
import models.Etudiant;
import models.Personne;
import models.Professionnel;
import utils.CSVReader;
import utils.CSVWriter;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.io.File;
import java.io.IOException;
import java.util.Map;

public class CsvDataController {
    public static Boolean insertData(File file, DataType type) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("gestion_sio");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        try {
            CSVReader csvReader = new CSVReader(file);
            if (DataChecker.checkData(csvReader.getFields(), type)) {
                switch (type) {
                    case ETUDIANT:
                        for (Map<String, String> map : csvReader.getValues()) {
                            Personne personne = new Personne();
                            Etudiant etudiant = new Etudiant();
                            personne.setNom(map.get("nom"));
                            personne.setPrenom(map.get("prenom"));
                            personne.setEmail(map.get("email"));
                            personne.setCivilite(map.get("civilite"));
                            etudiant.setPersonne(personne);
                            entityManager.persist(personne);
                            entityManager.persist(etudiant);
                        }
                        break;
                    case ENSEIGNANT:
                        for (Map<String, String> map : csvReader.getValues()) {
                            Personne personne = new Personne();
                            Enseignant enseignant = new Enseignant();
                            personne.setNom(map.get("nom"));
                            personne.setPrenom(map.get("prenom"));
                            personne.setEmail(map.get("email"));
                            personne.setCivilite(map.get("civilite"));
                            enseignant.setPersonne(personne);
                            entityManager.persist(personne);
                            entityManager.persist(enseignant);
                        }
                        break;
                    case PROFESSIONNEL:
                        for (Map<String, String> map : csvReader.getValues()) {
                            Personne personne = new Personne();
                            Professionnel professionnel = new Professionnel();
                            personne.setNom(map.get("nom"));
                            personne.setPrenom(map.get("prenom"));
                            personne.setEmail(map.get("email"));
                            personne.setCivilite(map.get("civilite"));
                            professionnel.setPersonne(personne);
                            entityManager.persist(personne);
                            entityManager.persist(professionnel);
                        }
                        break;
                    default:
                }
                entityManager.flush();
            } else return false;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        entityTransaction.commit();
        return true;
    }

    public static void exportData(File file, DataType type) {
        EntityManager entityManager = GestionSIO.getEntityManager();
        CSVWriter csvWriter = new CSVWriter(file);
        String[] columnsName = null;
        Object[][] data = null;
        switch (type) {
            case ETUDIANT:
//                Query query = entityManager.createQuery(
//                        "SELECT e.id, p.nom, p.prenom, p.email, p.telephone, " +
//                                "p.civilite, c.libelle, o.libelle FROM Etudiant e " +
//                                "JOIN Personne p ON p.id = e.personne.id " +
//                                "JOIN Classe c ON c.id = e.classe.id " +
//                                "JOIN Option o ON o.id = e.option.id");
//                Query query = entityManager.createQuery("SELECT p.nom, p.prenom FROM Personne p");
//                List<Object[]> etudiants = (List<Object[]>) query.getResultList();
//                columnsName = new String[]{"id", "nom", "prenom", "email", "telephone", "civilite", "classe", "option"};
//                columnsName = new String[]{"nom", "prenom"};
//                data = new Object[etudiants.size()][];
//                for (int i = 0; i < etudiants.size(); i++) data[i] = etudiants.get(i);
                break;
        }
        if (columnsName != null && data != null) csvWriter.write(columnsName, data);
    }
}