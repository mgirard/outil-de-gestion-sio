package controller;

import events.WindowEvents;
import main.GestionSIO;
import views.AbstractView;

import javax.swing.*;
import java.awt.*;

public class GeneralController {
    private JFrame jFrame;
    private HomeController homeController;
    private RegisterController registerController;
    private AdminController adminController;

    public GeneralController() {
        this.jFrame = new JFrame("");
        this.buildFrame();
    }

    private void buildFrame() {
        this.jFrame.addWindowListener(new WindowEvents());
        this.jFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.jFrame.setMinimumSize(new Dimension(800, 600));
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.jFrame.setLocation(screenSize.width / 2 - this.jFrame.getWidth() / 2, screenSize.height / 2 - this.jFrame.getHeight() / 2); // Center window in screen
        this.jFrame.setVisible(true);
        this.jFrame.pack();
    }

    public void initialize() {
        this.homeController = new HomeController(this);
        this.registerController = new RegisterController(this);
        this.adminController = new AdminController(this);
    }

    public void displayView(AbstractView view) {
        view.beforeDisplay();
        this.jFrame.setContentPane(view.getContent());
        this.setFrameTitle(view.getTitle());
        this.jFrame.pack();
    }

    public void setFrameTitle(String title) {
        this.jFrame.setTitle(title + " - " + GestionSIO.APP_NAME);
    }

    public void exit() {
        // Demander confirmation
        if (JOptionPane.showConfirmDialog(this.jFrame, "Quitter l'application\nEtes-vous sûr(e) ?", GestionSIO.APP_NAME, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }

    public HomeController getHomeController() {
        return homeController;
    }

    public RegisterController getRegisterController() {
        return registerController;
    }

    public AdminController getAdminController() {
        return adminController;
    }

    public JFrame getJFrame() {
        return jFrame;
    }
}