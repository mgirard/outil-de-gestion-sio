package controller;

import main.GestionSIO;
import models.Utilisateur;
import utils.BCrypt;
import views.Register;

import javax.swing.*;
import java.awt.event.*;

public class RegisterController implements ActionListener, MouseListener, KeyListener {
    private Register vue;
    private GeneralController generalController;

    public RegisterController(GeneralController controller) {
        this.vue = new Register();
        this.generalController = controller;
        this.vue.getRetourButton().addMouseListener(this);
        this.vue.getConnexionButton().addMouseListener(this);
        this.vue.getPasswordField().addKeyListener(this);
    }

    public void login() {
        try {
            String pseudo = this.vue.getPseudoField().getText();
            String password = String.valueOf(this.vue.getPasswordField().getPassword());
            Utilisateur user = Utilisateur.findOneByPseudo(pseudo);
            if (user != null && BCrypt.checkpw(password, user.getMotDePasse())) {
                GestionSIO.setUser(user);
                this.vue.getPseudoField().setText(null);
                this.vue.getPasswordField().setText(null);
                this.generalController.displayView(this.generalController.getHomeController().getVue());
            } else this.errorLogin();
        } catch (Exception e) {
            e.printStackTrace();
            this.errorLogin("Une erreur est survenue.");
        }
    }

    private void errorLogin(String message) {
        JOptionPane.showMessageDialog(this.generalController.getJFrame(), message, "Login", JOptionPane.ERROR_MESSAGE);
    }

    private void errorLogin() {
        this.errorLogin("Pseudo ou Mot de passe incorrect !");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == this.vue.getRetourButton()) {
            this.generalController.displayView(this.generalController.getHomeController().getVue());
        }
        if (e.getSource() == this.vue.getConnexionButton()) {
            if (GestionSIO.getUser().getId() == 0) {
                this.login(); // Try to login user
            }
            else {
                GestionSIO.setUser(new Utilisateur()); // Logout user
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getSource().equals(this.getVue().getPasswordField())) {
            if (e.getKeyCode() == 10) { // If have press "Enter"
                this.login();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public Register getVue() {
        return vue;
    }
}
