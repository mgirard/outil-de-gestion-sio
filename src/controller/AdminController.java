package controller;

import views.AdminData;
import views.InsertData;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class AdminController implements ActionListener, MouseListener {
    private AdminData vue;
    private GeneralController generalController;

    private InsertData insertData;

    public AdminController(GeneralController controller) {
        this.vue = new AdminData();
        this.generalController = controller;
        this.insertData = new InsertData();
        this.vue.getReturnButton().addMouseListener(this);
        this.vue.getInsertCsvData().addMouseListener(this);
    }

    public AdminData getVue() {
        return vue;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == vue.getReturnButton()) {
            this.generalController.displayView(this.generalController.getHomeController().getVue());
        } else if (e.getSource() == this.vue.getInsertCsvData()) {
            this.insertData.setVisible(true);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
