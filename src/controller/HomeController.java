package controller;

import data.DataType;
import main.GestionSIO;
import models.Utilisateur;
import views.Home;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

public class HomeController implements ActionListener, MouseListener {
    private Home vue;
    private GeneralController generalController;

    public HomeController(GeneralController controller) {
        this.vue = new Home();
        this.generalController = controller;
        this.vue.getLoginButton().addMouseListener(this);
        this.vue.getSearchButton().addMouseListener(this);
        this.vue.getAdminButton().addMouseListener(this);
        this.vue.getExportationDeLaTableButton().addActionListener(e -> {
            if (this.vue.getCategorieComboBox().getSelectedIndex() != -1) {
                JFileChooser chooser = new JFileChooser();
                chooser.setSelectedFile(new File(((DataType) this.vue.getCategorieComboBox().getSelectedItem()).getKey() + ".csv"));
                int choose = chooser.showDialog(this.generalController.getJFrame(), "Fichier de destination");
                if (choose == JFileChooser.APPROVE_OPTION) {
                    if (chooser.getSelectedFile() != null) {
                        this.vue.exportTable(chooser.getSelectedFile());
                    }
                }
            }
        });
    }

    public Home getVue() {
        return vue;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == vue.getLoginButton()) {
            if (GestionSIO.getUser().getId() != 0) {
                GestionSIO.setUser(new Utilisateur());
                this.generalController.getHomeController().getVue().setCategorie();
                this.generalController.getHomeController().getVue().updateLoginButton();
                this.generalController.getHomeController().getVue().updateAdminButton();
            } else {
                this.generalController.displayView(this.generalController.getRegisterController().getVue());
            }
        } else if (e.getSource() == vue.getSearchButton()) {
            vue.setTable();
        } else if (e.getSource() == this.vue.getAdminButton()) {
            if (GestionSIO.getUser().getRole() == 2) {
                this.generalController.displayView(this.generalController.getAdminController().getVue());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
