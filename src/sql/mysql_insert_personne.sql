--
-- Données de la table `personne`
--

INSERT INTO `personne` (`id`, `nom`, `prenom`, `email`, `telephone`, `civilite`) VALUES
(1, 'Le Fourn', 'Alex', 'alefourn@la-joliverie.com', '0610111213', 'Monsieur'),
(2, 'Rayer', 'Alex', 'arayer@la-joliverie.com', '0611121314', 'Monsieur'),
(3, 'Balestrat', 'Rudy', 'rbalestrat@la-joliverie.com', '0612131415', 'Monsieur'),
(4, 'Girard', 'Mathéo', 'mgirard@la-joliverie.com', '0613141516', 'Monsieur'),
(5, 'Bourgeois', 'Nicolas', 'nbourgeois@la-joliverie.com', '0613141516', 'Monsieur'),
(6, 'Contant', 'Nelly', 'ncontant@la-joliverie.com', '0614151617', 'Madame'),
(7, 'Le Pro', 'Professionnel', 'plepro@la-joliverie.com', '0617181920', 'Monsieur'),
(8, 'La Pro', 'Professionnel', 'plapro@la-joliverie.com', '0618192021', 'Madame');