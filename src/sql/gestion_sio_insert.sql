USE gestion_sio;
--
-- Données de la table `personne`
--
INSERT INTO `personne` (`id`, `nom`, `prenom`, `email`, `telephone`, `civilite`) VALUES
(1, 'Le Fourn', 'Alex', 'alefourn@la-joliverie.com', '0610111213', 'Monsieur'),
(2, 'Rayer', 'Alex', 'arayer@la-joliverie.com', '0611121314', 'Monsieur'),
(3, 'Balestrat', 'Rudy', 'rbalestrat@la-joliverie.com', '0612131415', 'Monsieur'),
(4, 'Girard', 'Mathéo', 'mgirard@la-joliverie.com', '0613141516', 'Monsieur'),
(5, 'Bourgeois', 'Nicolas', 'nbourgeois@la-joliverie.com', '0613141516', 'Monsieur'),
(6, 'Contant', 'Nelly', 'ncontant@la-joliverie.com', '0614151617', 'Madame'),
(7, 'Le Pro', 'Professionnel', 'plepro@la-joliverie.com', '0617181920', 'Monsieur'),
(8, 'La Pro', 'Professionnel', 'plapro@la-joliverie.com', '0618192021', 'Madame');

--
-- Déchargement des données de la table `enseignant`
--
INSERT INTO `enseignant` (`id`, `personne_id`) VALUES
(1, 5),
(2, 6);

--
-- Déchargement des données de la table `entreprise`
--
INSERT INTO `entreprise` (`id`, `nom`, `adresse`) VALUES
(1, 'La Poste', '10 Rue de la Poste'),
(2, 'L\'entreprise', '10 Rue de l\'Entreprise');

--
-- Déchargement des données de la table `classe`
--
INSERT INTO `classe` (`id`, `libelle`) VALUES
(1, '2SLAM'),
(2, '2SISR'),
(3, '1SLAM'),
(4, '1SISR'),
(5, '1SIOB'),
(6, '1SIOA');

--
-- Déchargement des données de la table `option`
--
INSERT INTO `option` (`id`, `libelle`) VALUES
(1, 'SLAM'),
(2, 'SISR');

--
-- Déchargement des données de la table `etudiant`
--
INSERT INTO `etudiant` (`id`, `personne_id`, `classe_id`, `option_id`) VALUES
(1, 1, 1, 1),
(2, 4, 1, 1),
(3, 2, 2, 2),
(4, 3, 2, 2);

--
-- Déchargement des données de la table `etudiant_classe`
--
INSERT INTO `etudiant_classe` (`etudiant_id`, `classe_id`, `annee`) VALUES
(1, 1, '2020-01-01'),
(2, 2, '2020-01-01'),
(3, 1, '2020-01-01'),
(4, 2, '2020-01-01');

--
-- Déchargement des données de la table `professionnel`
--
INSERT INTO `professionnel` (`id`, `personne_id`, `ancien_eleve`) VALUES
(1, 7, 0),
(2, 8, 1);

--
-- Déchargement des données de la table `emploi_maitre_stage`
--
INSERT INTO `emploi_maitre_stage` (`id`, `professionnel_id`, `entreprise_id`, `specialite`, `metier`, `annee`) VALUES
(1, 1, 1, 'SLAM', 'Développeur', '2020-01-01'),
(2, 2, 2, 'SISR', 'Administrateur Réseau', '2020-01-01');

--
-- Déchargement des données de la table `stage`
--
INSERT INTO `stage` (`id`, `etudiant_id`, `professionnel_id`, `entreprise_id`, `enseignant_id`, `debut`, `fin`, `techno`, `sujet`) VALUES
(1, 1, 1, 1, 1, '2020-01-01', '2020-03-01', 'Symfony, PHP, base de données MySql', 'Création d\'un site vitrine'),
(2, 2, 2, 2, 2, '2020-01-01', '2020-03-01', 'Cisco Packet Tracer', 'Configuration d\'un routeur');

--
-- Déchargement des données de la table `visite`
--
INSERT INTO `visite` (`id`, `stage_id`, `date`, `compte_rendu`, `oral`, `dispo_stage_premiere_annee`, `dispo_stage_deuxieme_annee`) VALUES
(1, 1, '2020-02-01', 'C\'est un très bonne élève qui travail pour un client. ', 1, 1, 0),
(2, 2, '2020-02-01', 'Blablablablabla', 1, 1, 1);