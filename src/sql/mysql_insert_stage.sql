--
-- Déchargement des données de la table `stage`
--

INSERT INTO `stage` (`id`, `etudiant_id`, `professionnel_id`, `entreprise_id`, `enseignant_id`, `debut`, `fin`, `techno`, `sujet`) VALUES
(1, 1, 1, 1, 1, '2020-01-01', '2020-03-01', 'Symfony, PHP, base de données MySql', 'Création d\'un site vitrine'),
(2, 2, 2, 2, 2, '2020-01-01', '2020-03-01', 'Cisco Packet Tracer', 'Configuration d\'un routeur');