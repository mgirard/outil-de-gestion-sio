--
-- Déchargement des données de la table `emploi_maitre_stage`
--

INSERT INTO `emploi_maitre_stage` (`id`, `professionnel_id`, `entreprise_id`, `specialite`, `metier`, `annee`) VALUES
(1, 1, 1, 'SLAM', 'Développeur', '2020-01-01'),
(2, 2, 2, 'SISR', 'Administrateur Réseau', '2020-01-01');