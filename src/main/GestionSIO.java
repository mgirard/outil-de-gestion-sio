package main;

import controller.GeneralController;
import models.Utilisateur;
import views.Load;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.swing.*;
import java.awt.*;

public class GestionSIO {
    private static GeneralController generalController = new GeneralController();
    private static EntityManager entityManager;
    public static final String APP_NAME = "Gestion SIO";
    public static Utilisateur user = new Utilisateur();

    public static void main(String[] args) {
        Load load = new Load();
        GestionSIO.getGeneralController().displayView(load);
        try {
            GestionSIO.entityManager = Persistence.createEntityManagerFactory("gestion_sio").createEntityManager();
            Utilisateur.createUserIfNotExist();
            GestionSIO.generalController.initialize();
            GestionSIO.generalController.displayView(generalController.getHomeController().getVue());
        } catch (Exception e) {
            System.out.println("Class : " + e.getClass());
            String msg = "An error as occurred";
            if (e instanceof PersistenceException) {
                msg = "Impossible de se connecter à la base de données.";
            }
            load.getLoadingBar().setVisible(false);
            load.getText().setForeground(Color.RED);
            load.getText().setText(msg);
            load.getText().setFont(new Font("Monospaced", Font.PLAIN, 20));
            GestionSIO.getGeneralController().setFrameTitle("Erreur");
            JOptionPane.showMessageDialog(GestionSIO.generalController.getJFrame(), msg, "Error.", JOptionPane.ERROR_MESSAGE);
        }
    }

    public static GeneralController getGeneralController() {
        return generalController;
    }

    public static EntityManager getEntityManager() {
        return entityManager;
    }

    public static EntityTransaction getEntityTransaction() {
        return entityManager.getTransaction();
    }

    public static Utilisateur getUser() {
        return user;
    }

    public static void setUser(Utilisateur user) {
        GestionSIO.user = user;
    }
}
