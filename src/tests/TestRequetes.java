package tests;

import models.Stage;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.sql.ResultSet;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TestRequetes {

    public static void main(String[] args) {
        EntityManager em;
        List<Object> desPersonnes;
        List<Stage> desStages;
        Iterator itr;

        System.out.println("\nDEBUT DES TESTS DE PERSISTANCE ");
        em = Persistence.createEntityManagerFactory("gestion_sio").createEntityManager();

        System.out.println("Liste des contact ayant donnée leur accord pour participer au jury pour la spécialité SLAM");
        Query query1 = em.createQuery("SELECT p.nom, p.prenom, p.email, p.telephone, p.civilite, e.nom, e.adresse FROM Visite v \n" +
                "JOIN Stage s on v.stage.id = s.id \n" +
                "JOIN Professionnel p2 on s.professionnel.id = p2.id\n" +
                "JOIN Personne p on p2.personne.id = p.id\n" +
                "JOIN EmploiMaitreStage ems on p2.id = ems.professionnel.id \n" +
                "JOIN Entreprise e on ems.entreprise.id = e.id\n" +
                "WHERE ems.specialite = 'SLAM' AND FUNC('YEAR',v.date) = 2020 AND v.oral = 1");
        desPersonnes = query1.getResultList();
        itr = desPersonnes.iterator();
        while (itr.hasNext()) {
            Object[] obj = (Object[]) itr.next();
            for (int i = 0; i < obj.length; i++) {
                System.out.println(String.valueOf(obj[i]));
            }
        }
        System.out.println("Test 1 effectué");

        System.out.println('\n');
        System.out.println("Liste des contact ayant donnée leur accord pour participer au jury pour la spécialité SISR");
        Query query2 = em.createQuery("SELECT p.nom, p.prenom, p.email, p.telephone, p.civilite, e.nom, e.adresse FROM Visite v \n" +
                "JOIN Stage s on v.stage.id = s.id \n" +
                "JOIN Professionnel p2 on s.professionnel.id = p2.id\n" +
                "JOIN Personne p on p2.personne.id = p.id\n" +
                "JOIN EmploiMaitreStage ems on p2.id = ems.professionnel.id \n" +
                "JOIN Entreprise e on ems.entreprise.id = e.id\n" +
                "WHERE ems.specialite = 'SISR' AND FUNC('YEAR',v.date) = 2020 AND v.oral = 1");
        desPersonnes = query2.getResultList();
        itr = desPersonnes.iterator();
        while (itr.hasNext()) {
            Object[] obj = (Object[]) itr.next();
            for (int i = 0; i < obj.length; i++) {
                System.out.println(String.valueOf(obj[i]));
            }
        }
        System.out.println("Test 2 effectué");

        System.out.println('\n');
        System.out.println("Liste des contact ayant donnée leur accord de principe pour accueillir un stagiaire pour la spécialité SISR pour les 1ère année");
        Query query3 = em.createQuery("SELECT p.nom, p.prenom, p.email, p.telephone, p.civilite, e.nom, e.adresse FROM Visite v\n" +
                "JOIN Stage s ON s.id = v.stage.id\n" +
                "JOIN Professionnel pr ON pr.id = s.professionnel.id \n" +
                "JOIN EmploiMaitreStage ems on pr.id = ems.professionnel.id \n" +
                "JOIN Personne p ON p.id = pr.personne.id\n" +
                "JOIN Entreprise e ON e.id = s.entreprise.id\n" +
                "WHERE ems.specialite = 'SISR' AND FUNC('YEAR',v.date) = 2020 AND v.dispoStagePremiereAnnee = 1");
        desPersonnes = query3.getResultList();
        itr = desPersonnes.iterator();
        while (itr.hasNext()) {
            Object[] obj = (Object[]) itr.next();
            for (int i = 0; i < obj.length; i++) {
                System.out.println(String.valueOf(obj[i]));
            }
        }
        System.out.println("Test 3 effectué");

        System.out.println('\n');
        System.out.println("Liste des contact ayant donnée leur accord de principe pour accueillir un stagiaire pour la spécialité SLAM pour les 1ère année");
        Query query4 = em.createQuery("SELECT p.nom, p.prenom, p.email, p.telephone, p.civilite, e.nom, e.adresse FROM Visite v\n" +
                "JOIN Stage s ON s.id = v.stage.id\n" +
                "JOIN Professionnel pr ON pr.id = s.professionnel.id \n" +
                "JOIN EmploiMaitreStage ems on pr.id = ems.professionnel.id \n" +
                "JOIN Personne p ON p.id = pr.personne.id\n" +
                "JOIN Entreprise e ON e.id = s.entreprise.id\n" +
                "WHERE ems.specialite = 'SLAM' AND FUNC('YEAR',v.date) = 2020 AND v.dispoStagePremiereAnnee = 1");
        desPersonnes = query4.getResultList();
        itr = desPersonnes.iterator();
        while (itr.hasNext()) {
            Object[] obj = (Object[]) itr.next();
            for (int i = 0; i < obj.length; i++) {
                System.out.println(String.valueOf(obj[i]));
            }
        }
        System.out.println("Test 4 effectué");

        System.out.println('\n');
        System.out.println("Liste des contact ayant donnée leur accord de principe pour accueillir un stagiaire pour la spécialité SISR pour les 2ème année");
        Query query5 = em.createQuery("SELECT p.nom, p.prenom, p.email, p.telephone, p.civilite, e.nom, e.adresse FROM Visite v\n" +
                "JOIN Stage s ON s.id = v.stage.id\n" +
                "JOIN Professionnel pr ON pr.id = s.professionnel.id \n" +
                "JOIN EmploiMaitreStage ems on pr.id = ems.professionnel.id \n" +
                "JOIN Personne p ON p.id = pr.personne.id\n" +
                "JOIN Entreprise e ON e.id = s.entreprise.id\n" +
                "WHERE ems.specialite = 'SISR' AND FUNC('YEAR',v.date) = 2020 AND v.dispoStageDeuxiemeAnnee = 1");
        desPersonnes = query3.getResultList();
        itr = desPersonnes.iterator();
        while (itr.hasNext()) {
            Object[] obj = (Object[]) itr.next();
            for (int i = 0; i < obj.length; i++) {
                System.out.println(String.valueOf(obj[i]));
            }
        }
        System.out.println("Test 5 effectué");

        System.out.println('\n');
        System.out.println("Liste des contact ayant donnée leur accord de principe pour accueillir un stagiaire pour la spécialité SLAM pour les 2ème année");
        Query query6 = em.createQuery("SELECT p.nom, p.prenom, p.email, p.telephone, p.civilite, e.nom, e.adresse FROM Visite v\n" +
                "JOIN Stage s ON s.id = v.stage.id\n" +
                "JOIN Professionnel pr ON pr.id = s.professionnel.id \n" +
                "JOIN EmploiMaitreStage ems on pr.id = ems.professionnel.id \n" +
                "JOIN Personne p ON p.id = pr.personne.id\n" +
                "JOIN Entreprise e ON e.id = s.entreprise.id\n" +
                "WHERE ems.specialite = 'SLAM' AND FUNC('YEAR',v.date) = 2020 AND v.dispoStageDeuxiemeAnnee = 1");
        desPersonnes = query6.getResultList();
        itr = desPersonnes.iterator();
        while (itr.hasNext()) {
            Object[] obj = (Object[]) itr.next();
            for (int i = 0; i < obj.length; i++) {
                System.out.println(String.valueOf(obj[i]));
            }
        }
        System.out.println("Test 6 effectué");

        System.out.println('\n');
        System.out.println("Liste des stagiaires encadrées par un contact données");
        Query query7 = em.createQuery("SELECT e.personne.nom, e.personne.prenom, e.personne.telephone, e.personne.email, \n" +
                "ems.specialite, FUNC('YEAR',ems.annee), s.debut, s.fin, s.sujet, s.techno  FROM Stage s\n" +
                "JOIN Etudiant e ON e.id = s.etudiant.id\n" +
                "JOIN Professionnel pr ON pr.id = s.professionnel.id\n" +
                "JOIN Personne p ON p.id = pr.personne.id\n" +
                "JOIN EmploiMaitreStage ems ON ems.professionnel.id = pr.id\n" +
                "WHERE s.enseignant.id = 1");
        desPersonnes = query7.getResultList();
        itr = desPersonnes.iterator();
        while (itr.hasNext()) {
            Object[] obj = (Object[]) itr.next();
            for (int i = 0; i < obj.length; i++) {
                System.out.println(String.valueOf(obj[i]));
            }
        }
        System.out.println("Test 7 effectué");

        System.out.println('\n');
        System.out.println("Liste des stagiaires encadrées par une organisation données");
        Query query8 = em.createQuery("SELECT e.personne.nom, e.personne.prenom, e.personne.telephone, e.personne.email, \n" +
                "ems.specialite, FUNC('YEAR',ems.annee), s.debut, s.fin, s.sujet, s.techno FROM Stage s\n" +
                "JOIN Etudiant e ON e.id = s.etudiant.id\n" +
                "JOIN Professionnel pr ON pr.id = s.professionnel.id\n" +
                "JOIN Personne p ON p.id = pr.personne.id\n" +
                "JOIN EmploiMaitreStage ems ON ems.professionnel.id = pr.id\n" +
                "JOIN Entreprise en ON en.id = s.entreprise.id \n" +
                "WHERE en.nom = 'La Poste'");
        desPersonnes = query8.getResultList();
        itr = desPersonnes.iterator();
        while (itr.hasNext()) {
            Object[] obj = (Object[]) itr.next();
            for (int i = 0; i < obj.length; i++) {
                System.out.println(String.valueOf(obj[i]));
            }
        }
        System.out.println("Test 8 effectué");

        System.out.println('\n');
        System.out.println("Consultation des anciens stages");
        Query query9 = em.createQuery("SELECT s.debut, s.fin, s.techno, s.sujet FROM Stage s " +
                "WHERE FUNC('YEAR',s.debut) < FUNC('YEAR', CURRENT_DATE )"
        );
        desPersonnes = query9.getResultList();
        itr = desPersonnes.iterator();
        while (itr.hasNext()) {
            Object[] obj = (Object[]) itr.next();
            for (int i = 0; i < obj.length; i++) {
                System.out.println(String.valueOf(obj[i]));
            }
        }
        System.out.println("Test 9 effectué");

        System.out.println('\n');
        System.out.println("Consultation des stages");
        Query query10 = em.createQuery("SELECT s.debut, s.fin, s.techno, s.sujet FROM Stage s ");
        desPersonnes = query10.getResultList();
        itr = desPersonnes.iterator();
        while (itr.hasNext()) {
            Object[] obj = (Object[]) itr.next();
            for (int i = 0; i < obj.length; i++) {
                System.out.println("" + obj[i].toString());
                System.out.println();
            }
        }
        System.out.println("Test 10 effectué");

        Query query100 = em.createQuery("SELECT s.debut, s.fin, s.techno, s.sujet FROM Stage s ");
        desPersonnes = query100.getResultList();
        itr = desPersonnes.iterator();
        String[] columns = {"Début", "Fin", "Techno", "Sujet"};
        int iterateur = 0;
        Object[][] test = new Object[desPersonnes.size()][];
        while (itr.hasNext()) {
            Object[] obj = (Object[]) itr.next();
            test[iterateur] = obj;
            iterateur++;
        }
        for (int i = 0; i< test.length; i++) {
            for (int y = 0; y < test[i].length; y++){
                System.out.println(test[i][y]);
            }
        }
    }
}
