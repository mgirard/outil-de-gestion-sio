package tests;

import models.Personne;
import models.Utilisateur;
import utils.BCrypt;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.Scanner;

public class DataInsertion {

    private static EntityManager entityManager = Persistence.createEntityManagerFactory("gestion_sio").createEntityManager();
    private static EntityTransaction entityTransaction = entityManager.getTransaction();

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        DataInsertion.insertUser();
    }

    public static void insertUser() {
        DataInsertion.entityTransaction.begin();
        Personne personne = null;
        boolean u = true;
        while (u) {
            System.out.print("\n\nUtiliser une personne déjà existante ? [n] y/n : ");
            String r = DataInsertion.scanner.nextLine();
            if (r.equals("n") || r.equals("")) {
                personne = new Personne();
                System.out.print("\nNom de la personne [Nom] : ");
                String v = DataInsertion.scanner.nextLine();
                personne.setNom(v.equals("") ? "Nom" : v);
                System.out.print("\nPrénom de la personne [Prénom] : ");
                v = DataInsertion.scanner.nextLine();
                personne.setPrenom(v.equals("") ? "Prénom" : v);
                entityManager.persist(personne);
                u = false;
            } else if (r.equals("y")) {
                boolean c = true;
                while (c) {
                    System.out.print("\nId de la personne : ");
                    try {
                        int id = Integer.parseInt(DataInsertion.scanner.nextLine());
                        EntityManager entityManager = Persistence.createEntityManagerFactory("gestion_sio").createEntityManager();
                        Query query = entityManager.createQuery("SELECT p FROM Personne p WHERE p.id = :id")
                                .setParameter("id", id);
                        if (query.getResultList().size() > 0 && query.getSingleResult() != null) {
                            personne = (Personne) query.getSingleResult();
                            c = false;
                        } else System.out.println("\nId non trouvé !");
                    } catch (Exception e) {
                        System.out.println("\nId non trouvé !");
                    }
                }
                u = false;
            } else System.out.println("\nRéponse inconnu.");
        }
        Utilisateur user = new Utilisateur();
        System.out.print("\nPseudo de l'utilisateur [Pseudo] : ");
        String v = DataInsertion.scanner.nextLine();
        user.setPseudo(v.equals("") ? "Pseudo" : v);
        System.out.print("\nMot de passe de l'utilisateur [password] : ");
        v = DataInsertion.scanner.nextLine();
        user.setMotDePasse(BCrypt.hashpw(v.equals("") ? "password" : v, BCrypt.gensalt(10)));
        u = true;
        while (u) {
            System.out.print("\nRôle de l'utilisateur [2] 1/2 : ");
            String s = DataInsertion.scanner.nextLine();
            if (s.equals("")) {
                user.setRole(2);
                u = false;
            } else {
                try {
                    int r = Integer.parseInt(s);
                    if (r == 1 || r == 2) {
                        user.setRole(r);
                        u = false;
                    } else System.out.print("\nRôle inconnu.");
                } catch (Exception e) {
                    System.out.print("\nRôle inconnu.");
                }
            }
        }
        user.setPersonne(personne);
        entityManager.persist(user);
        entityManager.flush();
        entityTransaction.commit();
        System.out.println(user.getPseudo() + " inserted !");
    }
}
