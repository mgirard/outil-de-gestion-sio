package tests;

import utils.BCrypt;

public class PasswordManagerTest {

    public static void main(String[] args) {
        String password = "password";
        String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt(10));
        System.out.println(hashedPassword);
        if (BCrypt.checkpw(password, hashedPassword)) {
            System.out.println("It's same password");
        }
    }
}
