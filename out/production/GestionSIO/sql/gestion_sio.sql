SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;

CREATE USER IF NOT EXISTS 'gestion_sio_user'@'localhost' IDENTIFIED BY 'MotDePasseUltraSecure';
GRANT ALL PRIVILEGES ON gestion_sio.* TO 'gestion_sio_user'@'localhost';

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gestion_sio`
--

DROP DATABASE IF EXISTS gestion_sio;
CREATE DATABASE IF NOT EXISTS gestion_sio;
USE gestion_sio;

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

DROP TABLE IF EXISTS `personne`;
CREATE TABLE IF NOT EXISTS `personne`
(
    `id`        int(11)      NOT NULL AUTO_INCREMENT,
    `nom`       varchar(255) NOT NULL,
    `prenom`    varchar(255) NOT NULL,
    `email`     varchar(255),
    `telephone` varchar(255),
    `civilite`  varchar(255),
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `enseignant`
--

DROP TABLE IF EXISTS `enseignant`;
CREATE TABLE IF NOT EXISTS `enseignant`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT,
    `personne_id` int(11) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY `fk_personne` (`personne_id`) REFERENCES personne (id)
) ENGINE = InnoDB
  DEFAULT CHARSET utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

DROP TABLE IF EXISTS `entreprise`;
CREATE TABLE IF NOT EXISTS `entreprise`
(
    `id`      int(11)      NOT NULL AUTO_INCREMENT,
    `nom`     varchar(255) NOT NULL,
    `adresse` varchar(255),
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `option`
--

DROP TABLE IF EXISTS `option`;
CREATE TABLE IF NOT EXISTS `option`
(
    `id`      int(11)      NOT NULL AUTO_INCREMENT,
    `libelle` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `professionnel`
--

DROP TABLE IF EXISTS `professionnel`;
CREATE TABLE IF NOT EXISTS `professionnel`
(
    `id`           int(11) NOT NULL AUTO_INCREMENT,
    `personne_id`  int(11) NOT NULL,
    `ancien_eleve` tinyint(1) DEFAULT 0,
    PRIMARY KEY (`id`),
    FOREIGN KEY `fk_personne` (`personne_id`) REFERENCES personne (id)
) ENGINE = InnoDB
  DEFAULT CHARSET utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur`
(
    `id`           int(11)      NOT NULL AUTO_INCREMENT,
    `pseudo`       varchar(255) NOT NULL,
    `mot_de_passe` text         NOT NULL,
    `role`         tinyint(1)   NOT NULL,
    `personne_id`  int(11)      NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY `fk_personne` (`personne_id`) REFERENCES personne (id)
) ENGINE = InnoDB
  DEFAULT CHARSET utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

DROP TABLE IF EXISTS `classe`;
CREATE TABLE IF NOT EXISTS `classe`
(
    `id`      int(11)      NOT NULL AUTO_INCREMENT,
    `libelle` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `emploi_maitre_stage`
--

DROP TABLE IF EXISTS `emploi_maitre_stage`;
CREATE TABLE IF NOT EXISTS `emploi_maitre_stage`
(
    `id`               int(11)      NOT NULL AUTO_INCREMENT,
    `professionnel_id` int(11)      NOT NULL,
    `entreprise_id`    int(11)      NOT NULL,
    `specialite`       varchar(255) NOT NULL,
    `metier`           varchar(255) NOT NULL,
    `annee`            date         NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY `fk_professionnel` (`professionnel_id`) REFERENCES professionnel (id),
    FOREIGN KEY `fk_entreprise` (`entreprise_id`) REFERENCES entreprise (id)
) ENGINE = InnoDB
  DEFAULT CHARSET utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

DROP TABLE IF EXISTS `etudiant`;
CREATE TABLE IF NOT EXISTS `etudiant`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT,
    `personne_id` int(11) NOT NULL,
    `classe_id`   int(11),
    `option_id`   int(11),
    PRIMARY KEY (`id`),
    FOREIGN KEY `fk_personne` (`personne_id`) REFERENCES personne (id),
    FOREIGN KEY `fk_classe` (`classe_id`) REFERENCES classe (id),
    FOREIGN KEY `fk_option` (`option_id`) REFERENCES `option` (id)
) ENGINE = InnoDB
  DEFAULT CHARSET utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `etudiant_classe`
--

DROP TABLE IF EXISTS `etudiant_classe`;
CREATE TABLE IF NOT EXISTS `etudiant_classe`
(
    `etudiant_id` int(11) NOT NULL,
    `classe_id`   int(11) NOT NULL,
    `annee`       date    NOT NULL,
    PRIMARY KEY (`etudiant_id`, `classe_id`),
    FOREIGN KEY `fk_etudiant` (`etudiant_id`) REFERENCES etudiant (id),
    FOREIGN KEY `fk_classe` (`classe_id`) REFERENCES classe (id)
) ENGINE = InnoDB
  DEFAULT CHARSET utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `stage`
--

DROP TABLE IF EXISTS `stage`;
CREATE TABLE IF NOT EXISTS `stage`
(
    `id`               int(11) NOT NULL AUTO_INCREMENT,
    `etudiant_id`      int(11) NOT NULL,
    `professionnel_id` int(11) NOT NULL,
    `entreprise_id`    int(11) NOT NULL,
    `enseignant_id`    int(11),
    `debut`            date    NOT NULL,
    `fin`              date    NOT NULL,
    `techno`           text,
    `sujet`            varchar(255),
    PRIMARY KEY (`id`),
    FOREIGN KEY `fk_etudiant` (`etudiant_id`) REFERENCES etudiant (id),
    FOREIGN KEY `fk_professionnel` (`professionnel_id`) REFERENCES professionnel (id),
    FOREIGN KEY `fk_entreprise` (`entreprise_id`) REFERENCES entreprise (id),
    FOREIGN KEY `fk_enseignant` (`enseignant_id`) REFERENCES enseignant (id)
) ENGINE = InnoDB
  DEFAULT CHARSET utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `visite`
--

DROP TABLE IF EXISTS `visite`;
CREATE TABLE IF NOT EXISTS `visite`
(
    `id`                         int(11) NOT NULL AUTO_INCREMENT,
    `stage_id`                   int(11) NOT NULL,
    `date`                       date    NOT NULL,
    `compte_rendu`               text    NOT NULL,
    `oral`                       tinyint(1),
    `dispo_stage_premiere_annee` tinyint(1),
    `dispo_stage_deuxieme_annee` tinyint(1),
    PRIMARY KEY (`id`),
    FOREIGN KEY `fk_stage` (`stage_id`) REFERENCES stage (id)
) ENGINE = InnoDB
  DEFAULT CHARSET utf8mb4;

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;